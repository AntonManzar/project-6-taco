package tacoCloud.taco.model;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import java.util.List;

@Data
public class Taco {

    @NotNull
    @Size(min=5, message = "Имя должно быть не короче 5 символов")
    private String name;

    @NotNull
    @Size(min=1, message = "Список ингредиентов должен быть не короче 1 символа")
    private List<Ingredient> ingredients;
}
