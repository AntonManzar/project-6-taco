package tacoCloud.taco.model;

import lombok.Data;
import tacoCloud.taco.enums.Type;

@Data
public class Ingredient {
    private final String id;
    private final String name;
    private final Type type;
}
