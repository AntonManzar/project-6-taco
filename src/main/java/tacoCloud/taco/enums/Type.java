package tacoCloud.taco.enums;

public enum Type {
    WRAP, PROTEIN, VEGGIES, CHEESE, SAUCE
}
