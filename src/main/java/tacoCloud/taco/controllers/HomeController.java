package tacoCloud.taco.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import tacoCloud.taco.model.Taco;

@Controller
public class HomeController {

    @GetMapping("/")
    public String home(@ModelAttribute("taco") Taco taco, Model model) {
        taco.setName("MyTaco");
        model.addAttribute("man", "Mark");
        return "home";
    }
}
